"""
This set of functions takes a network defined as a list of node
coordinates and builds estimates of the quality of links based on
the distance between nodes.
"""
from random import gauss
from math import sqrt
from my_utils.normal_distr import NormalDistr
def get_distance(net, a, b):
    """Calculate the distance between nodes a and b in the list.

    net  -- list of (x,y) node coordinates
    a, b -- indeces of nodes in the net list.
    """
    x_dist = net[a][0] - net[b][0]
    y_dist = net[a][1] - net[b][1]
    return sqrt(x_dist*x_dist + y_dist*y_dist)
    
def get_rssi(distance):
    """Estimate an RSSI based on the distance - FAKE.

    If distance < 30, sample N(-30, -10).
    If distance >30 <60, sample N(-60, -30).
    If distance >60, sample N(-80, -10).
    """
    if distance < 0:
        raise ValueError("Distance must be positive")
    if distance < 11:   #connected region   
        return gauss(-30, 2)
    elif distance < 25: #transitional region
        return gauss(-70, 15)
    else:               #disconnected region
        return gauss(-90, 5)

def get_etx(distance):
    if distance < 0:
        raise ValueError("Distance must be positive")
    if distance < 11:   #connected region   
        return NormalDistr(5, 2)
    elif distance < 25: #transitional region
        return NormalDistr(25, 10)
    else:               #disconnected region
        return NormalDistr(50, 5)

def net_link_quality_analyser(net):
    """
    Returns a connectivity matrix of NxN, where each element
    holds an RSSI value and an ETX Normal distribution.

    The links are  considered bi-directional so the matrix is
    symmetric.
    """
    
    conn_matrix = [[None for i in xrange(len(net))] for j in xrange(len(net))]

    for n_a in xrange(len(net)):
        for n_b in xrange(len(net)):
            el = {}
            if n_a == n_b:
                el = None
            else:
                dist_a_b = get_distance(net, n_a, n_b)
                el['rssi'] = get_rssi(dist_a_b)
                el['etx'] = get_etx(dist_a_b)
            conn_matrix[n_a][n_b] = el
            conn_matrix[n_b][n_a] = el

    return conn_matrix

