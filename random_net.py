"""
This creates a network where the nodes are randomly
placed on the area. Therefore, there are no 
guarantees on the number of neighbours or connectivity.
"""

from random import randint
from test_gen import SimScriptBuilder, udgm_ct_loss_radio
def random_net(net_size, radio_range, num_nodes, script_file):
    """
    Quick test, creates a network of @net_size, with @num_nodes 
    communicating over @radio_range

    Returns the list of nodes
    """
    net_diag = int(sqrt(2)*net_size)
    store = NodeStore(net_diag, radio_range)
    for i in xrange(num_nodes):
         n = Node(randint(0,net_size), randint(0,net_size))
         neighbs = store.find_neighbours(n)
         for nb in neighbs:
             nb.neighbs.append(n)
             n.neighbs.append(nb)
         store.add(n)
    nodes = store.get_nodes()
    script_builder = SimScriptBuilder(script_file, udgm_ct_loss_radio)
    script_builder.write_headers()
    script_builder.add_mote(0, 0, 0, 'sky1')
    for (i,n) in enumerate(nodes):
        script_builder.add_mote(n.x, n.y, i+1, 'sky2')
    script_builder.done_adding_motes()
    script_builder.enable_logging('sim_log.pcap')
    script_builder.set_script('script1_ctrl.js')
    script_builder.finalise()

if __name__ == '__main__':
    random_net(500, 50, 50, 'random.csc')
