"""
This generates a simulation file for Cooja.
It has a boilerplate header.

What can be controlled:
    - the radio model (select b/w UDGM and UDGM constant loss)
    - the TX and RX success rate
    - the TX power
    - the number and position of the nodes
"""

"""
In the header we control the random seed.
The value can be "generated" or a number
"""
header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<simconf>\n<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mrm</project>\n<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mspsim</project>\n<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/avrora</project>\n<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mobility</project>\n<simulation>\n<title>Data collection network using IPv6 and RPL</title>\n<delaytime>0</delaytime>\n<randomseed>%s</randomseed>\n<motedelay_us>5000000</motedelay_us><events>\n<logoutput>40000</logoutput>\n</events>\n"


"""
The radio_model header has three parameters:
- radio range and interference range
- the type of the radio model.

Right now you can choose b/w udgm and udgm with constant loss.
"""
radio_model = "<radiomedium>\n%s\n<transmitting_range>%d</transmitting_range>\n<interference_range>%d</interference_range>\n<success_ratio_tx>1.0</success_ratio_tx>\n<success_ratio_rx>1.0</success_ratio_rx>\n</radiomedium>"

udgm_radio = "org.contikios.cooja.radiomediums.UDGM"
udgm_ct_loss_radio = "org.contikios.cooja.radiomediums.UDGMConstantLoss"

"""
Desription of a mote application.
Parameters of string (s.format() is used)
%mote_type identifier
%path to file
%filename (without extension)
%firmware type (e.g sky, cooja)
"""
mote_desc = " <motetype>\n\
        {mote_class}\n\
        <identifier>{mote_type}</identifier>\n\
        <description>Sky Mote Type #sky1</description>\n\
        <source EXPORT=\"discard\">{path}/{filename}.c</source>\n\
        <commands EXPORT=\"discard\">make {filename}.{firmware} TARGET={firmware}</commands>\n\
        <firmware EXPORT=\"copy\">{path}/{filename}.{firmware}</firmware>\n\
        <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.IPAddress</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspSerial</moteinterface>\n"
#        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>\n\
#        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface>\n\
#        </motetype>\n"

"""
The mote template is used when adding nodes to the simulation.
It has three parameters, in this exact order:
    x position (int), y position (int),
    mote interface class (string)
    mote id  (int), mote type ("sky1" for base station, "sky2" for regular)
"""
mote_template = "<mote>\n<breakpoints />\n<interface_config>\norg.contikios.cooja.interfaces.Position\n<x>%d</x>\n<y>%d</y>\n<z>0.0</z>\n</interface_config>\n<interface_config>\n%s\n<id>%s</id>\n</interface_config>\n<motetype_identifier>%s</motetype_identifier>\n</mote>"

"""
It is possible to log the packets into pcap format.
The parameter is the path to the file where the packets will be logged.
"""
packet_logger = "<plugin>\norg.contikios.cooja.plugins.RadioLoggerHeadless\n<plugin_config>\n<pcap_file>%s</pcap_file>\n</plugin_config></plugin>\n"
debug_logger = "<plugin>\norg.contikios.cooja.plugins.LogListenerHeadless\n<plugin_config>\n<append>%s</append></plugin_config></plugin>\n"

"""
The simulation is controlled by a Javascript script.

The length of the experiment (in ms) is the parameter.
"""
script_runner = "<plugin>\norg.contikios.cooja.plugins.ScriptRunner\n<plugin_config><script>\n\
TIMEOUT(%d);\n\
while (true) {\n\
YIELD();\n\
}\n\
</script><active>true</active></plugin_config></plugin>"

script_runner2 = "<plugin>\norg.contikios.cooja.plugins.ScriptRunner\n<plugin_config><script>\n\
 //import Java Package to JavaScript\n\
 importPackage(java.io);\n\
\n\
 // Use JavaScript object as an associative array\n\
 outputs = new FileWriter(%s);\n\
\n\
 while (true) {\n\
    //Write to file.\n\
    outputs.write(time + \" \" + msg + \"\\n\");\n\
\n\
    try{\n\
        //This is the tricky part. The Script is terminated using\n\
        // an exception. This needs to be caught.\n\
        YIELD();\n\
    } catch (e) {\n\
        //Close files.\n\
        outputs.close();\n\
        //Rethrow exception again, to end the script.\n\
        throw('test script killed');\n\
    }\n\
 }\n\
</script><active>true</active></plugin_config></plugin>"

footer = "</simconf>"

mote_classes = {'sky':'org.contikios.cooja.mspmote.SkyMoteType',\
                'cooja':'org.contikios.cooja.contikimote.ContikiMoteType'}

class SimScriptBuilder:
    def __init__(self, filename, radio_type, radio_range):
        self.filename = filename
        self.radio = radio_type
        self.radio_range = radio_range
        self.header = header % "generated"
        self.radio_model = radio_model % (self.radio, self.radio_range, self.radio_range)
        self.mote_desc_list = ""
        self.mote_list = ""
        self.packet_logger = None
        self.debug_logger = None
        self.script_runner = None

    def set_random_seed(self, random_seed):
        self.header = header % str(random_seed)

    def add_mote_app(self, mote_type, app_path, fw='sky'):
        fields = app_path.split('/')
        filename = fields[-1].split('.')[0]
        path = '/'.join(fields[:-1])
        self.mote_desc_list += mote_desc.format(mote_class=mote_classes[fw], mote_type=mote_type, path=path, filename=filename, firmware=fw)
        if fw == 'cooja':
            self.mote_desc_list += "<moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiMoteID</moteinterface>\n"
        else:
            self.mote_desc_list += "<moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>\n"
        self.mote_desc_list += "</motetype>\n"

    
    def add_mote(self, mote_x, mote_y, mote_id, mote_type):
        print "Adding %d @ (%d,%d), type=%s" % (mote_id, mote_x, mote_y, mote_type)
        if mote_type.startswith('cooja'):
            mote_id_iface = "org.contikios.cooja.contikimote.interfaces.ContikiMoteID"
        else:
            mote_id_iface = "org.contikios.cooja.mspmote.interfaces.MspMoteID"
        self.mote_list += mote_template % (mote_x, mote_y, mote_id_iface, mote_id, mote_type)


    def enable_logging(self, path):
        self.packet_logger = packet_logger % path

    def enable_debug(self, path):
        self.debug_logger = debug_logger % path

    def set_exp_length(self, length_ms):
        self.script_runner = script_runner % length_ms

    def generate(self):
        if not (self.radio_model and self.script_runner):
            raise FormatException()
        if len(self.mote_desc_list) == 0 or len(self.mote_list) == 0:
            raise FormatException()
        f = open(self.filename, 'w')
        f.write(self.header)
        f.write(self.radio_model)
        f.write(self.mote_desc_list)
        f.write(self.mote_list)
        f.write("</simulation>")
        if self.packet_logger:
            f.write(self.packet_logger)
        if self.debug_logger:
            f.write(self.debug_logger)
        if self.script_runner:
            f.write(self.script_runner)
        f.write(footer)
        f.close()

